variable "cloudflare_api_key" {
  description = "The Cloudflare API key"
  type        = string
}

variable "zone_name" {
  description = "The DNS zone name which will be added"
  type        = string
}

variable "plan" {
  description = "The name of the commercial plan to apply to the zone"
  type        = string
}

variable "type" {
  description = "A full zone implies that DNS is hosted with Cloudflare. A partial zone is typically a partner-hosted zone or a CNAME setup"
  type        = string
}

variable "proton_CNAME1_name" {
  description = "The name of the CNAME1 record for Protonmail"
  type        = string
}

variable "proton_CNAME1_value" {
  description = "The name of the CNAME1 record for Protonmail"
  type        = string
}

variable "proton_CNAME2_name" {
  description = "The name of the CNAME2 record for Protonmail"
  type        = string
}

variable "proton_CNAME2_value" {
  description = "The name of the CNAME2 record for Protonmail"
  type        = string
}

variable "proton_CNAME3_name" {
  description = "The name of the CNAME3 record for Protonmail"
  type        = string
}

variable "proton_CNAME3_value" {
  description = "The name of the CNAME3 record for Protonmail"
  type        = string
}

variable "proton_MX1_name" {
  description = "The name of the MX1 record for Protonmail"
  type        = string
}

variable "proton_MX1_value" {
  description = "The name of the MX1 record for Protonmail"
  type        = string
}

variable "proton_MX2_name" {
  description = "The name of the MX2 record for Protonmail"
  type        = string
}

variable "proton_MX2_value" {
  description = "The name of the MX2 record for Protonmail"
  type        = string
}

variable "proton_TXT1_name" {
  description = "The name of the TXT1 record for Protonmail"
  type        = string
}

variable "proton_TXT1_value" {
  description = "The name of the TXT1 record for Protonmail"
  type        = string
}

variable "proton_TXT2_name" {
  description = "The name of the TXT2 record for Protonmail"
  type        = string
}

variable "proton_TXT2_value" {
  description = "The name of the TXT2 record for Protonmail"
  type        = string
}

variable "proton_TXT3_name" {
  description = "The name of the TXT3 record for Protonmail"
  type        = string
}

variable "proton_TXT3_value" {
  description = "The name of the TXT3 record for Protonmail"
  type        = string
}

variable "http3" {
  description = "Accelerates HTTP requests by using QUIC, which provides encryption and performance improvements compared to TCP and TLS"
  type        = string
}

variable "zero_rtt" {
  description = "Improves performance for clients who have previously connected to your website"
  type        = string
}

variable "ipv6" {
  description = "Enable IPv8 support and gateway"
  type        = string
}

variable "websockets" {
  description = "Allow WebSockets connections to your origin server"
  type        = string
}

variable "onion" {
  description = "Onion Routing allows routing traffic from legitimate users on the Tor network through Cloudflare’s onion services rather than exit nodes, thereby improving privacy of the users and enabling more fine-grained protection"
  type        = string
}

variable "pseudo_ipv4" {
  description = "Adds an IPv4 header to requests when a client is using IPv6, but the server only supports IPv4"
  type        = string
}

variable "ip_geolocation" {
  description = "Include the country code of the visitor location with all requests to your website"
  type        = string
}

variable "max_upload" {
  description = "The amount of data visitors can upload to your website in a single request"
  type        = number
}

variable "always_use_https" {
  description = "Redirect all requests with scheme “http” to “https”. This applies to all http requests to the zone"
  type        = string
}

variable "min_tls_version" {
  description = "Only allow HTTPS connections from visitors that support the selected TLS protocol version or newer"
  type        = string
}

variable "encryption" {
  description = "Opportunistic Encryption allows browsers to benefit from the improved performance of HTTP/2 by letting them know that your site is available over an encrypted connection. Browsers will continue to show “http” in the address bar, not “https"
  type        = string
}

variable "tls_1_3" {
  description = "Enable the latest version of the TLS protocol for improved security and performance"
  type        = string
}

variable "automatic_https_rewrites" {
  description = "Automatic HTTPS Rewrites helps fix mixed content by changing “http” to “https” for all resources or links on your web site that can be served with HTTPS"
  type        = string
}

variable "cache_level" {
  description = "Determine how much of your website’s static content you want Cloudflare to cache. Increased caching can speed up page load time"
  type        = string
}

variable "browser_cache_ttl" {
  description = "Determine the length of time Cloudflare instructs a visitor’s browser to cache files. During this period, the browser loads the files from its local cache, speeding up page loads"
  type        = number
}

variable "always_online" {
  description = "Keep your website online for visitors when your origin server is unavailable. Cloudflare serves limited copies of web pages available from the Internet Archive’s Wayback Machine. Enabling this Service will share some of your website’s information with the Internet Archive in order to make this feature functional. By enabling this Service, you agree to the Supplemental Terms for Always Online"
  type        = string
}

variable "development_mode" {
  description = "Temporarily bypass our cache allowing you to see changes to your origin server in realtime"
  type        = string
}
